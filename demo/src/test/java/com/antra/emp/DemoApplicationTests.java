package com.antra.emp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.antra.emp.entity.EmployeeEntity;
import com.antra.emp.repository.EmployeeRepository;

@DataJpaTest
class DemoApplicationTests {
	@Autowired
	EmployeeRepository repo;

	@Test
	public void TestToFindEmployee() {
		int id = 101;
		boolean flag = repo.existsById(id);
		assertTrue(flag);
	}
	@Test
	public void TestToFindAllEmployees() {
		List<EmployeeEntity> list=repo.findAll();
		int count=10;
		assertThat(list).hasSize(count);
	}
}
