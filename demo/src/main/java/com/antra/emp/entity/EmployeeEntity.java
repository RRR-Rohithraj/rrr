package com.antra.emp.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmployeeEntity {
	@Id
	private Integer empId;
	private String empName;
	private Long empMobile;
	private String empEmail;
	

	public EmployeeEntity() {
		super();
	}

	public EmployeeEntity(Integer empId, String empName, Long empMobile, String empEmail) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empMobile = empMobile;
		this.empEmail = empEmail;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Long getEmpMobile() {
		return empMobile;
	}

	public void setEmpMobile(Long empMobile) {
		this.empMobile = empMobile;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

}
