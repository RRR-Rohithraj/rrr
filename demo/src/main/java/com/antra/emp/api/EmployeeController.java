package com.antra.emp.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.antra.emp.Exeption.EmployeeDoesNotExist;
import com.antra.emp.model.EmployeeModel;
import com.antra.emp.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeService serv;
	@PostMapping("/add")
	public String newEmployee(@RequestBody EmployeeModel emp) {
		return serv.addEmployee(emp);
	}
	@PutMapping("/update")
	public String updateEmployee(@RequestBody EmployeeModel emp) {
		return serv.updateEmployee(emp);
	}
	@DeleteMapping("/delete/{id}")
	public String deleteEmployee(@PathVariable int id) {
		return serv.deleteEmployee(id);
	}
	@GetMapping("/employee/{id}")
	public EmployeeModel getEmployee(@PathVariable int id) {
		EmployeeModel emp=serv.getEmployee(id);
		if(emp==null) {
			throw new EmployeeDoesNotExist("Employee with given id does not exist");
		}
		else {
			return emp;
		}
	}

}
