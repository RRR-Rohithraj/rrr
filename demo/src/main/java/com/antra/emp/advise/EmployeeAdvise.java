package com.antra.emp.advise;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.antra.emp.Exeption.EmployeeDoesNotExist;

@RestControllerAdvice
public class EmployeeAdvise {
	@ExceptionHandler(EmployeeDoesNotExist.class)	
	public ResponseEntity<String>  exHandler1(EmployeeDoesNotExist e) {
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String>  exHandler2(Exception e) {
		return new ResponseEntity<String>("Error due to request, Check the request url", HttpStatus.BAD_REQUEST);
	}

}

