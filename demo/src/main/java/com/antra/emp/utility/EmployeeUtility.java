package com.antra.emp.utility;

import org.springframework.beans.BeanUtils;

import com.antra.emp.entity.EmployeeEntity;
import com.antra.emp.model.EmployeeModel;

public class EmployeeUtility {
	public static EmployeeModel  entityToModel(EmployeeEntity emp) {
		EmployeeModel e=new EmployeeModel();
		BeanUtils.copyProperties(emp , e);
		return e;
	}
	public static EmployeeEntity  modelToEntity(EmployeeModel emp) {
		EmployeeEntity e=new EmployeeEntity();
		BeanUtils.copyProperties(emp , e);
		return e;
	}

}
